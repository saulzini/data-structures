// HashingLinear.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <iostream>
#define SIZE 10;

using namespace std;

int Hash(int key){
    return key%SIZE;
}

int probe(int H[], int key){
    int index = Hash(key);
    int i=0;

    while ( H[(index+i) % 10] != 0 )
    {
        i++;
    }
    return (index+i) % SIZE;
}

void insert(int H[],int key){
    int index = Hash(key);

    if (H[index] != 0){
        index = probe(H,key);
    }
    H[index]=key;

}

int search(int H[],int key){
    int index = Hash(key);

	int i = 0;
	while (H[(index + i) % 10] != key)
	{
		i++;
	}
    return (index + i) % SIZE;
}


int main()
{
    int HT[10]={0};

    insert(HT,12);
    insert(HT,25);
    insert(HT,35);
    insert(HT,26);

    cout<<search(HT,35)<<endl;
    cout<<search(HT,26)<<endl;

    return 0;
}
