// Tree.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <iostream>
#include "Queue.h"
#include "Stack.h"
using namespace std;

struct Node* root = NULL;

void TreeCreate() {
	struct Node* p, * t;
	int x;
	struct Queue q;
	create(&q, 100);

	cout << "Enter root value" << endl;
	cin >> x;
	root = (struct Node*) malloc(sizeof(struct Node));
	root->data = x;
	root->lchild = root->rchild = NULL;
	enqueue(&q, root);

	while (!isEmpty(q)) {
		p = dequeue(&q);

		cout << "Enter left child of " << p->data << endl;
		cin >> x;

		if (x != -1) {
			t = (struct Node*) malloc(sizeof(struct Node));
			t->data = x;
			t->lchild = t->rchild = NULL;

			p->lchild=t;
			enqueue(&q, t);
		}

		cout << "Enter right child of " << p->data << endl;
		cin >> x;

		if (x != -1) {
			t = (struct Node*) malloc(sizeof(struct Node));
			t->data = x;
			t->lchild = t->rchild = NULL;

			p->rchild = t;
			enqueue(&q, t);
		}

	}

}

void preorder(struct Node* p) {

	if (p) {
		cout << p->data << endl;
		preorder(p->lchild);
		preorder(p->rchild);
	}

}

void inorder(struct Node* p) {

	if (p) {
		inorder(p->lchild);
		cout << p->data << endl;
		inorder(p->rchild);
	}

}

void postorder(struct Node* p) {

	if (p) {
		postorder(p->lchild);
		postorder(p->rchild);
		cout << p->data << endl;
	}

}

void IterativePreorder(struct Node* p) {
	struct Stack stk;

	StackCreate(&stk, 100);

	while (p || !isEmptyStack(stk)) {
		if (p) {
			cout << p->data << endl;
			push(&stk, p);
			p = p->lchild;

		}
		else {
			p = pop(&stk);
			p = p->rchild;
		}
	}
}


void IterativeInorder(struct Node* p) {
	struct Stack stk;

	StackCreate(&stk, 100);

	while (p || !isEmptyStack(stk)) {
		if (p) {		
			push(&stk, p);
			p = p->lchild;

		}
		else {
			p = pop(&stk);
			cout << p->data << endl;
			p = p->rchild;
		}
	}
}
//BFS 
void LevelOrder(struct Node* p) {
	struct Queue q;
	create(&q, 100);
	cout << p->data<< endl;
	enqueue(&q, p);

	while ( !isEmpty(q)  )
	{
		p = dequeue(&q);
		if (p->lchild) {
			cout << p->lchild->data << endl;
			enqueue(&q,p->lchild);
		}

		if (p->rchild) {
			cout << p->rchild->data << endl;
			enqueue(&q, p->rchild);
		}

	}
}

int count(struct Node* p) {
	int x = 0,y=0;
	if (p) {
		return count(p->lchild) + count(p->rchild) +1;
	}
	return 0;
}

int countLeafNodes(struct Node* p) {

	if (p) {
		int x = countLeafNodes(p->lchild);
		int y = countLeafNodes(p->rchild);

		if (p->lchild == NULL && p->rchild == NULL) {
			return x + y + 1;
		}
		else {
			return x + y;
		}
		
	}
	return 0;
}

int height(struct Node* p) {
	int x = 0, y = 0;
	if (p) {
		x = height(p->lchild);
		y = height(p->rchild);

		if (x > y) {
			return x + 1;
		}
		else {
			return y + 1;
		}
	}
	return 0;
}

int main()
{
	TreeCreate();
	cout<<"count "<<count(root)<<endl;
	cout<<"height " << height(root) << endl;
	cout<<"count leaf nodes "<<countLeafNodes(root)<<endl;
	//cout << "Pre order" << endl;
	//IterativePreorder(root);
	//cout << "In order" << endl;
	//IterativeInorder(root);
	/*cout << "Breath first search" << endl;
	LevelOrder(root);*/

	/*
	cout << "In order" << endl;
	inorder(root);
	cout << "Post order" << endl;
	postorder(root);
	*/
}

// Ejecutar programa: Ctrl + F5 o menú Depurar > Iniciar sin depurar
// Depurar programa: F5 o menú Depurar > Iniciar depuración

// Sugerencias para primeros pasos: 1. Use la ventana del Explorador de soluciones para agregar y administrar archivos
//   2. Use la ventana de Team Explorer para conectar con el control de código fuente
//   3. Use la ventana de salida para ver la salida de compilación y otros mensajes
//   4. Use la ventana Lista de errores para ver los errores
//   5. Vaya a Proyecto > Agregar nuevo elemento para crear nuevos archivos de código, o a Proyecto > Agregar elemento existente para agregar archivos de código existentes al proyecto
//   6. En el futuro, para volver a abrir este proyecto, vaya a Archivo > Abrir > Proyecto y seleccione el archivo .sln
