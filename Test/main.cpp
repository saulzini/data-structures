#include <iostream>

using namespace std;

void fun(int n){
	if (n>0){
		cout << n<<endl;
		fun(n-1);
	}
}

int main(){
	cout << "HELLO WORLD" << std::endl;
	int x =3;
	fun(x);
	return 0;
}