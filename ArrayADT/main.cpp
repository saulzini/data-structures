#include <iostream>

using namespace std;

struct Array {
	int *A;
	int size;
	int length;
};

void Display(struct Array arr){
	cout<<"Elements in the array"<<endl;
	for(int i=0; i<arr.length; i++){
		cout<<arr.A[i]<<endl;
	}
}

void Append(struct Array *arr,int x){
	
	if (arr->length < arr->size){
		arr-> A[ arr->length++  ] = x;
	}
	
}

void Insert(struct Array *arr,int index,int x){
	
	if (index >= 0 && index <= arr->length ){
		for(int i= arr->length; i>index; i--){
			arr->A[i] = arr->A[i-1];
		}
		arr->A[index]=x;
		arr->length++;
	}
}

void Delete(struct Array *arr, int index ){
	if (index >= 0 && index <= arr->length ){
		
		for (int i=index; i<arr->length-1; i++){
			arr->A[i] = arr->A[i+1];
		}
		arr->length--;
		
	}
}

int LinearSearch(struct Array *arr,int element){
	for(int i=0; i < arr->length; i++ ){
		if (arr->A[i] == element){
			return i;
		}
	}
	return -1;

}

int RBinarySearch(int arr[],int l,int h,int key){
	
	if ( l <= h){
		int mid = (l + h)/2;
		
		if (arr[mid] == key){
			return mid;
		}
		else if (arr[mid] < key ){
			return RBinarySearch(arr,mid+1,h,key);
		}
		else {
			return RBinarySearch(arr,l,mid-1,key);
		}
	}
	return -1;
}



void InsertSort(struct Array *arr, int value){
	if (arr->length == arr->size){
		return;
	}
	int i=arr->length-1;
	while (i>=0 && arr->A[i] > value){
		arr->A[i+1] = arr->A[i];
		i--;
	}
	
	arr->A[i+1]=value;
	arr->length++;
}

int isSorted(struct Array *arr){
	for(int i=0; i<arr->length-1; i++){
		if (arr->A[i] > arr->A[i+1])
			return 0;
	}
	return 1;
}

void swap(int *x,int *y)
{
	int temp=*x;
	*x=*y;
	*y=temp;
}

void reArrange(struct Array *arr){
	int i,j;
	i=0;
	j= arr->length;
	
	while(i<j){
		while(arr->A[i] <0 ){
			i++;
		}
		while(arr->A[i] <0 ){
			j--;
		}
		if (i<j) swap(&arr->A[i],&arr->A[j] );
	}
}

struct Array* Merge (struct Array *arr1,struct Array *arr2){
	int i,j,k;
	i=j=k=0;
	struct Array *arr3 = (struct Array *) malloc(sizeof(struct Array));
	arr3->A =new int [10];
	while(i<arr1->length && j<arr2->length){
		if (arr1->A[i] < arr2->A[j] ){
			arr3->A[k] = arr1->A[i];
			i++;
			k++;
		}
		else {
			arr3->A[k] = arr2->A[j];
			k++;
			j++;
		}
	}
	for(;i<arr1->length; i++){
		arr3->A[k++] = arr1->A[i];
	}
	for(;j<arr2->length; j++){
		arr3->A[k++] = arr2->A[j];
	}
	
	arr3->length = arr1->length + arr2->length;
	arr3->size=10;
	return arr3;	
}


struct Array* Intersection (struct Array *arr1,struct Array *arr2){
	int i,j,k;
	i=j=k=0;
	struct Array *arr3 = (struct Array *) malloc(sizeof(struct Array));
	arr3->A =new int [10];
	while(i<arr1->length && j<arr2->length){
		if (arr1->A[i] < arr2->A[j] ){
			i++;
		}
		else  if(arr2->A[j] < arr1->A[i] ){
			arr3->A[k] = arr2->A[j];
			j++;
		}
		else if(arr1->A[i] == arr2->A[j] ) {
			arr3->A[k] = arr1->A[i];
			i++;
			j++;
			k++;
		}
	}
	
	arr3->length = k;
	arr3->size=10;
	return arr3;	
}


struct Array* Union (struct Array *arr1,struct Array *arr2){
	int i,j,k;
	i=j=k=0;
	struct Array *arr3 = (struct Array *) malloc(sizeof(struct Array));
	arr3->A =new int [10];
	while(i<arr1->length && j<arr2->length){
		if (arr1->A[i] < arr2->A[j] ){
			arr3->A[k] = arr1->A[i];
			i++;
			k++;
		}
		else  if(arr2->A[j] < arr1->A[i] ){
			arr3->A[k] = arr2->A[j];
			k++;
			j++;
		}
		else {
			arr3->A[k] = arr1->A[i];
			i++;
			j++;
			k++;
		}
	}
	
	for(;i<arr1->length; i++){
		arr3->A[k++] = arr1->A[i];
	}
	for(;j<arr2->length; j++){
		arr3->A[k++] = arr2->A[j];
	}
	
	arr3->length = k;
	arr3->size=10;
	return arr3;	
}


struct Array* Difference (struct Array *arr1,struct Array *arr2){
	int i,j,k;
	i=j=k=0;
	struct Array *arr3 = (struct Array *) malloc(sizeof(struct Array));
	arr3->A =new int [10];
	while(i<arr1->length && j<arr2->length){
		if (arr1->A[i] < arr2->A[j] ){
			arr3->A[k] = arr1->A[i];
			i++;
			k++;
		}
		else  if(arr2->A[j] < arr1->A[i] ){
			j++;
		}
		else {
			i++;
			j++;
		}
	}
	
	for(;i<arr1->length; i++){
		arr3->A[k++] = arr1->A[i];
	}
	
	
	arr3->length = k;
	arr3->size=10;
	return arr3;	
}

int main(){
	
	struct Array arr;
	struct Array arr2;
	
	int n,i;
	arr.A= new int [10];
	arr2.A= new int [10];
	n = 5;
	arr.length =0;
	
	for (i=0; i<n; i++){
		arr.A[i] = i;
		arr2.A[i] = i+1;
	}
	arr.length = n;
	arr2.length = n;
	
//	Append(&arr,10);
//	Insert(&arr,5,6);
//	Display(arr);
//	Delete(&arr,1);
//	Display(arr);
//	
//	cout<<"Finding 1:"<<LinearSearch(&arr,1)<<endl;
//	cout<<"Finding 10:"<<LinearSearch(&arr,10)<<endl;
//	
//	cout<<"Finding 1:"<<RBinarySearch(arr.A,0,arr.length-1,1)<<endl;
//	cout<<"Finding 10:"<<RBinarySearch(arr.A,0,arr.length-1,10)<<endl;
//	cout<<"Finding 6:"<<RBinarySearch(arr.A,0,arr.length-1,6)<<endl;
//	
//	InsertSort(&arr,1);
//	Display(arr);
	
//	cout<<"Sorted:"<<isSorted(&arr)<<endl;
//	
	Display(arr);
	Display(arr2);
	
	struct Array *arr3 = Difference(&arr,&arr2);
	Display(*arr3);
//	

	return 0;
}