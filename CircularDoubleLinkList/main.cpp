#include <iostream>

using namespace std;

struct Node{
  int data;
  struct Node *prev,*next;
}*first;

void create(int A[],int n){
  struct Node *last = NULL;
  struct Node *p = (struct Node*)malloc(sizeof(struct Node));

  p->data = A[0];
  p->prev=p->next= NULL;

  first = p;
  first->next = first;
  first->prev = first;
  last =first;
  
  for(int i=1; i<n; i++){
    p = (struct Node*)malloc(sizeof(struct Node));
    p->data = A[i];
    p->prev = last;
    p->next = first;
    first->prev = p;
    last->next = p;
    last=p;
  }

}

void Display(struct Node *p){
  cout<<"Display"<<endl;
  do {
    cout<<p->data<<endl;
    p= p->next;
  }while(p != first);

}

void DisplayBack(struct Node *p){
  cout<<"Display Back"<<endl;
  do {
    p= p->prev;
    cout<<p->data<<endl;
  }while(p != first);

}

int length(struct Node *p){
  int length = 0;
  
  do {
    length++;
    p= p->next;
  }while(p != first);

  return length;
}

void Insert(struct Node *p,int index,int data){
  if ( index <0 && index > length(p)){
    return;
  }

  struct Node *q = (struct Node*)malloc(sizeof(struct Node));

  if (index ==0){    
    q->data = data;
    q->prev = first->prev;
    q->next = first;
    first->prev->next =q;
    first->prev = q;
    first = q;
  }
  else {
    for(int i=0; i<index; i++){
      p = p->next;
    }
    
    q->data = data;
    q->prev = p->prev;
    p->prev->next = q;
    p->prev = q;
    q->next = p;
  }
}

int Delete(struct Node *p,int index){
  int data = -1;
  if ( index <0 && index > length(p)){
    return data;
  }
  
  if (index ==0){
    p->prev->next = p->next;
    p->next->prev = p->prev;
    data = p->data;
    free(first);
    p = p->next;
    first = p;
    
  }
  else {
    
    for(int i=0; i<index; i++){
      p = p->next;
    }
  
    p->prev->next = p->next;
    p->next->prev = p->prev;
    data = p->data;
    p = p->next;
  }
  return data;
}

int main(){

  int A[] = {1,2,3,4,5};
  create(A,5);
	Display(first);
  Insert(first,0,0);
  Display(first);
  Insert(first,1,9);
  Display(first);
  cout <<"deleted:"<< Delete(first,0)<<endl;
  Display(first);
  cout <<"deleted:"<< Delete(first,1)<<endl;
  Display(first);
	return 0;
}