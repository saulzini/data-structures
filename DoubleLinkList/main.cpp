#include <iostream>
using namespace std;

struct Node {
  struct Node *prev;
  int data;
  struct Node *next;
} *first = NULL;

void create(int A[],int n){
  struct Node *t,*last;
  
  first = (struct Node*) malloc(sizeof(struct Node));

  first->data = A[0];
  first->next = NULL;
  first->prev = NULL;
  last = first;
  
  for(int i=1; i<n; i++){
    t = (struct Node*) malloc(sizeof(struct Node));
    t->data = A[i];
    t->next = last->next;
    t->prev = last;
    last->next = t;
    last = t;
  }

}

void display(struct Node *p){
  cout<<"Display"<<endl;
  while(p){
    cout<<p->data<<endl;
    p=p->next;
  }
}

int length(struct Node *p){
  int length = 0;
  while(p){
    length++;
    p=p->next;
  }
  return length;
}

void insert(struct Node *p,int index,int data){
  cout<< "Insert "<<data<< " in pos:"<<index<<endl;
  struct Node *q = (struct Node*)malloc(sizeof(struct Node));
  
  if (index == 0){
    q->data = data;
    q->prev = NULL;
    q->next = first;
    first->prev = q;
    first = q;
  }
  else {
    for (int i=0; i<index-1; i++){
      p=p->next;
    }
    q->data = data;
    q->prev = p;
    q->next = p->next;
    if (p->next){
      p->next->prev=q;
    }
    p->next = q;
  }
}

int Delete(struct Node *p, int index){
  
  int result = -1;
  if (index < 1 || index > length(p)){
    return -1;
  }
  
  if (index ==1){

    first = first->next;
    if (first){
      first->prev = NULL;
    }
    result = first->data;
    free(p);
    
  }
  else {

    for(int i=0; i<index-1;i++){
      p=p->next;
    }
    
    p->prev->next = p->next;
    if (p->next){      
      p->next->prev = p->prev;
    }
   
    result = p->data;
    free(p);
  }
  return result;
}

void reverse(struct Node *p){
  struct Node *t;
  while(p != NULL){
    t = p->next;
    p->next = p->prev;
    p->prev = t;
    p=p->prev;
    
    if (p!=NULL && p->next == NULL){
      first = p;
    }
    
  }

}

int main(){
	
  int A[] = {2,3,4,5,6};
  create(A,5);
  display(first);
  insert(first,1,1);
  insert(first,6,7);
  display(first);
  cout<<"delete:"<<Delete(first,7)<<endl;
  display(first);
  cout<<"length"<<length(first)<<endl;
  reverse(first);
  display(first);
//  cout<<"delete:"<<Delete(first,6);
//  display(first);
	return 0;
}