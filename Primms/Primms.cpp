// Primms.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <iostream>
#define I INT_MAX

using namespace std;

int cost[][8]=
 {{I,I,I,I,I,I,I,I},
 {I,I,25,I,I,I,5,I},
 {I,25,I,12,I,I,I,10},
 {I,I,12,I,8,I,I,I},
 {I,I,I,8,I,16,I,14},
 {I,I,I,I,16,I,20,18},
 {I,5,I,I,I,20,I,I},
 {I,I,10,I,14,18,I,I}};
// near
int near[8]={I,I,I,I,I,I,I,I};
// Solution array
int t[2][6]; 


int main()
{
    int k,u,v,n = 7,min = I;

    // Searching for the min value in the upper diagonal
    for (size_t i = 0; i <= n; i++)
    {
        for (size_t j = 0; j <= n; j++)
        {
            if (cost[i][j] < min ){
                min = cost[i][j];
                u=i;
                v=j;
            }

        }
        
    }
    // Including in the result
    t[0][0] = u; 
    t[1][0] = v; 
    // Including in the near
    near[u] = near[v] = 0;

    for (size_t i = 0; i <= n; i++)
    {
        if (near[i] != 0){
            // adding to smal
            if (cost[i][u] < cost[i][v] )
                near[i] = u;
            else 
                near[i] = v;

        }
        
    }

    // get remaining edges
    for (size_t i = 1; i < n-1; i++)
    {
        // scan near array and get the less value
        min = I;
        for (size_t j = 0; j <= n; j++)
        {
            if ( near[j] != 0 &&  cost[j][near[j]] < min ){
                k = j;
                min = cost[j][near[j]];
            }
        }
        // Adding to result
        t[0][i] = k;
        t[1][i] = near[k];
        near[k] = 0;
        
        // update near array
        for (size_t j = 1; j <= n; j++)
        {
            if ( near[j] != 0 && cost[j][k] < cost[j][near[j]] )
                near[j]=k;
        }
        
    }

    for (size_t i = 0; i < n-1; i++)
    {
        cout << t[0][i] << ","<< t[1][i] <<endl;
    }
    
        

}