// AVLTrees.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <iostream>

using namespace std;
struct Node {
    struct Node *left;
    int data;
    int height;
    struct Node *right;
}*root = NULL;

int NodeHeight(struct Node *p){
    int hleft,hright;

    hleft = p && p->left ? p->left->height:0;
    hright = p && p->right ? p->right->height:0;

    return hleft > hright ? hleft +1 : hright+1;
}

int balancefactor(struct Node *p){
    int hleft,hright;

    hleft = p && p->left ? p->left->height:0;
    hright = p && p->right ? p->right->height:0;

    return hleft - hright;
}


struct Node* LLRotation(struct Node *p){
    struct Node *pLeft = p->left;
    struct Node *pLeftRight = pLeft->right;

    // Assign the right pointer to the left child the parent  
    pLeft->right = p;
    // Assign the left pointer of the parent the right child of the left pointer
    p->left = pLeftRight;

    // Calculating the heights
    p->height = NodeHeight(p);
    pLeft->height = NodeHeight(pLeft);

    // Change the root pointer to the new parent
    if (root == p){
        root = pLeft;
    }
    // return the new parent
    return pLeft;
}

struct Node* LRRotation(struct Node *p){
    struct Node *pLeft = p->left;
    struct Node *pLeftRight = pLeft->right;

    // Assign the right pointer to the left child right left pointer  
    pLeft->right = pLeftRight->left;
    // Assign the left pointer of the parent the right child of the left right pointer right pointer
    p->left = pLeftRight->right;

    // Pointing the new left and right for the pleftright
    pLeftRight ->left = pLeft;
    pLeftRight ->right = p;

    // Calculating the heights
    pLeft->height = NodeHeight(pLeft);
    p->height = NodeHeight(p);
    pLeftRight->height = NodeHeight(pLeftRight);

    // Change the root pointer to the new parent
    if (root == p){
        root = pLeftRight;
    }
    // return the new parent
    return pLeftRight;
}

struct Node* RRRotation(struct Node *p){
    struct Node *pRight = p->right;
    struct Node *pRightLeft = pRight->left;

    // Assign the right pointer to the right child the parent  
    pRight->left = p;
    // Assign the right pointer of the parent the right child of the right pointer
    p->right = pRightLeft;

    // Calculating the heights
    p->height = NodeHeight(p);
    pRight->height = NodeHeight(pRight);

    // Change the root pointer to the new parent
    if (root == p){
        root = pRight;
    }
    // return the new parent
    return pRight;
}

struct Node* RLRotation(struct Node *p){
    struct Node *pRight = p->right;
    struct Node *pRightLeft = pRight->left;

    // Assign the right pointer to the right child right right pointer  
    pRight->left = pRightLeft->right;
    // Assign the right pointer of the parent the right child of the right right pointer right pointer
    p->right = pRightLeft->left;

    // Pointing the new right and right for the pRightLeft
    pRightLeft ->right = pRight;
    pRightLeft ->left = p;

    // Calculating the heights
    p->height = NodeHeight(p);
    pRight->height = NodeHeight(pRight);
    pRightLeft->height = NodeHeight(pRightLeft);

    // Change the root pointer to the new parent
    if (root == p){
        root = pRightLeft;
    }
    // return the new parent
    return pRightLeft;
}

struct Node* recursiveInsert(struct Node *p,int key) {
	
	if (p == NULL) {
		struct Node *t = (struct Node*) malloc(sizeof(struct Node));
		t->data = key;
        t->height = 1;
		t->left = t->right = NULL;
		return t;
	}

	if (key < p->data) {
		p->left = recursiveInsert(p->left,key);
	}
	else if (key > p->data) {
		p->right = recursiveInsert(p->right,key);
	}
    p->height = NodeHeight(p);

    // Single
    if ( balancefactor(p) == 2 && balancefactor(p->left) == 1  ){
        return LLRotation(p);
    }
    // double
    else if ( balancefactor(p) == 2 && balancefactor(p->left) == -1  ){
        return LRRotation(p);
    }
    // single
    else if ( balancefactor(p) == -2 && balancefactor(p->right) == -1  ){
        return RRRotation(p);
    }
    // double
    else if ( balancefactor(p) == -2 && balancefactor(p->right) == 1  ){
        return RLRotation(p);
    }

	return p;
}

int main()
{
    // LL Rotation
    // root = recursiveInsert(root,10);
    // recursiveInsert(root,5);
    // recursiveInsert(root,2);

    // LR Rotation
    // root = recursiveInsert(root,50);
    // recursiveInsert(root,10);
    // recursiveInsert(root,20);

    // RR Rotation
    // root = recursiveInsert(root,5);
    // recursiveInsert(root,10);
    // recursiveInsert(root,15);

    // RL Rotation
    root = recursiveInsert(root,10);
    recursiveInsert(root,30);
    recursiveInsert(root,20);



}

// Ejecutar programa: Ctrl + F5 o menú Depurar > Iniciar sin depurar
// Depurar programa: F5 o menú Depurar > Iniciar depuración

// Sugerencias para primeros pasos: 1. Use la ventana del Explorador de soluciones para agregar y administrar archivos
//   2. Use la ventana de Team Explorer para conectar con el control de código fuente
//   3. Use la ventana de salida para ver la salida de compilación y otros mensajes
//   4. Use la ventana Lista de errores para ver los errores
//   5. Vaya a Proyecto > Agregar nuevo elemento para crear nuevos archivos de código, o a Proyecto > Agregar elemento existente para agregar archivos de código existentes al proyecto
//   6. En el futuro, para volver a abrir este proyecto, vaya a Archivo > Abrir > Proyecto y seleccione el archivo .sln
