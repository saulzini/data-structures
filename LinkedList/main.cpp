#include <iostream>
#include <stdlib.h>

using namespace std;

struct Node{
  int data;
  struct Node *next;
}*first = NULL,*second=NULL,*third=NULL;

void create(int A[],int n){
  struct Node *t,*last;
  
  first = (struct Node *)malloc(sizeof(struct Node));
  first->data = A[0];
  first->next = NULL;

  last = first;

  for(int i=1;i<n; i++){
    t = (struct Node*) malloc(sizeof(struct Node));
    t->data=A[i];
    t->next = NULL;
    
    last->next = t;
    last = t;
  }
  
}

void create2(int A[],int n){
  struct Node *t,*last;
  
  second = (struct Node *)malloc(sizeof(struct Node));
  second->data = A[0];
  second->next = NULL;

  last = second;

  for(int i=1;i<n; i++){
    t = (struct Node*) malloc(sizeof(struct Node));
    t->data=A[i];
    t->next = NULL;
    
    last->next = t;
    last = t;
  }
  
}

void Display(struct Node *p){
  while( p!=NULL){
    cout<<p->data<<endl;
    p = p->next;
  }

}

void DisplayRecursive(struct Node *p){
  if (p!= NULL){
    cout<<p->data<<endl;
    DisplayRecursive(p->next);
  }

}

int count(struct Node *p){
  int count=0;
  
  while(p != NULL){
    p=p->next;
    count++;
  }
  
  return count;
}

int countRecursive(struct Node *p){
  if (p==NULL)
    return 0;
  else {
    return countRecursive(p->next)+1;
  }

}

int sum(struct Node *p){
  int count = 0;
  
  while(p!=NULL){
    count += p->data;
    p=p->next;
  }
  
  return count;
}

int sumRecursive(struct Node *p){
  if (p==NULL)
    return 0;
  
  return p->data+sumRecursive(p->next);
}

int max(struct Node *p){
  int max = INT32_MIN;
  
  while (p != NULL){
    if (p->data > max){
      max = p->data;
    }
    p = p->next;
    
  }

    return max;
}

int maxRecursive(struct Node *p){
  if (p==NULL){
    return INT32_MIN;
  }
  
  int aux = maxRecursive(p->next);
  if ( aux > p->data ){
    return aux;
  }
  return p->data;
}

Node* lSearch(struct Node *p, int key){
  struct Node *q = NULL;
  while (p != NULL){
    if (key == p->data){
//      pointing to next
      q->next=p->next;
      p->next = first;
      first= p;

      return p;
    }
    q=p;
    p=p->next;
  }
  
  return NULL;
}

Node* searchRecursive(struct Node *p, int key){
  if (p == NULL){
    return NULL;
  }
  else if (p->data == key){
    return p;
  }
  else{
    return searchRecursive(p->next,key);
  }
}

void insert(struct Node *p,int index,int data){
  
  if ( index <0  || index > count(p) )
    return;
  
  struct Node *t = (struct Node*) malloc(sizeof(struct Node));
  t->data = data;

  if ( index ==0 ){
    t->next = first;
    first = t;
  }
  else {
    
    for(int i=0; i<index-1; i++){
      p=p->next;
    }
    t->next = p->next;
    p->next = t;
  }
}

void sortedInsert(struct Node *p,int data){
  struct Node *t = (struct Node*) malloc(sizeof( struct Node));
  struct Node *q;
  
  t->data = data;
  t->next = NULL;
  
  if (first==NULL){
      first = t;
  }
  else {
    while (p != NULL && p->data < data){
      q=p;
      p=p->next;
    }
  //    In case there is one element
    if (p==first){
      p->next = first;
      first = p;
    }
    else {
      t->next = q->next;
      q->next=t;
    }
  }
}

int Delete(struct Node *p, int index){
  struct Node *q;
  int x =-1;

  if (index < 1 || index > count(p)){
    return -1;
  }

  if (index ==1){
    q= first;
    x = first->data;
    first = first->next;
    free(q);
    return x;
  }
  else {
    for(int i=0; i<index-1; i++){
      q = p;
      p= p->next;

    }

    q->next = p->next;
    x= p->data;
    free(p);
    return x;
  }
}

int isSorted(struct Node *p){
  
  int x = INT32_MIN;
  
  while (p!=NULL){
    if (p->data < x){
      return false;
    }
    x = p->data;
    p=p->next;
  }
  
  return true;
}

void removeDuplicates(struct Node *p){
  struct Node *q;
  
  int currentData;

  while(p != NULL){
    currentData = p->data;
    q=p->next;
    if (q != NULL && currentData == q->data){
      p->next = q->next;
      free(q);
    }
    p = p->next;
  }
}

void reverseElements(struct Node *p){

  int *A;
  struct Node *initial = p;
  int size = count(p);
  A= (int *) malloc(sizeof(int) * size);
  
  for(int i=0; i< size; i++)
  {
    A[i] = p->data;
    p = p->next;
  }
  
  p = initial;
  for(int i=size-1; i >= 0; i--){
    p->data = A[i];
    p=p->next;
  }
  initial = NULL;
  free(A);
}

void reverseLinks(struct Node *p){
  
  struct Node *q = NULL,*r = NULL;
  
  while(p!=NULL){
    
    r=q;
    q=p;    
    p= p->next;
    q->next = r;
  }
  first = q;
}

void recursiveReverse(struct Node *q, struct Node *p){

  if (p==NULL){
    first = q;
  }
  else {
    recursiveReverse(p,p->next);
    p->next = q;
  }
}

void concat(struct Node *p,struct Node *q){
  third = p;
  while (p->next != NULL){
    p=p->next;
  }
  p->next= q;
  
}

void merge(struct Node *p,struct Node *q){

  struct Node *last;

  if (p->data < q->data){
      third = last =p;
      p=p->next;
      third->next = NULL;
    }
    else {
      third = last =q;
      q=q->next;
      third->next = NULL;
    }
  

  while( p != NULL && q != NULL ){
    if (p->data < q->data){
      last->next = p;
      last = p;
      p=p->next;
      last->next = NULL;
    }
    else {
      last->next = q;
      last = q;
      q= q->next;
      last->next = NULL;
    }
  }

  if (p) last->next = p;
  if (q) last->next = q;

}

int isLoop(struct Node *f){
  struct Node *p,*q;
  p=q=f;
  
  do{
    p= p->next;
    q=q->next;
    if (q->next){
      q=q->next;
    }

  }while(p && q && p!= q);
  
  if (p==q){
    return 1;
  }
  return 0;

}

int findMiddle(struct Node *p){
  int middle = -1;
  struct Node *q;
  q=p;
  
  while(q){
    q = q->next;
    
    if (q){
      q = q->next;
    }
    if (q){
      p = p->next;
    }
  }
  middle = p->data;

  return middle;
}

int main(){
	
  struct Node *t1,*t2;
  int A[] = {10,20,30,40,50};
  int B[] = {5,15,25,35,45};
  
  create(A,5);
  cout<<"find middle:"<<findMiddle(first)<<endl;
//  creating looped list
//  t1 = first ->next->next;
//  t2= first->next->next->next->next;
//  t2->next = t1;
//   cout<<"is loop "<<isLoop(first)<<endl;
  

//  create2(B,5);
//  cout<<"first"<<endl;
//  Display(first);
//  cout<<"second"<<endl;
//  Display(second);
////  concat(first,second);
//  merge(first,second);
//  cout<<"merged"<<endl;
//  Display(third);

  

//  cout<<"Display Recursive"<<endl; 
//  DisplayRecursive(first);
//  cout<<"Count:"<<count(first)<<endl;
//  cout<<"Count Recursive:"<<countRecursive(first)<<endl;
//
//  cout<<"Sum:"<<sum(first)<<endl;
//  cout<<"Sum Recursive:"<<sumRecursive(first)<<endl;
//
//  cout<<"Max:"<<max(first)<<endl;
//  cout<<"Max Recursive:"<<maxRecursive(first)<<endl;
//
// 
//  cout<<"Search Recursive:"<<searchRecursive(first,10)<<endl;
//  cout<<"Search & move first:"<<lSearch(first,10)<<endl;
//  Display(first);
  
//  cout << "Inserting 1 at position 0"<<endl;
//  insert(first,0,1);
//  cout << "Inserting 2 at position 1"<<endl;
//  insert(first,1,2);
//  cout<<"Display"<<endl;
//  Display(first);
//  cout<<"Insert 4"<<endl;
//  sortedInsert(first,4);


//  Display(first);
//  
//  cout<<"Delete:"<<Delete(first,2)<<endl;
//  cout<<"Sorted:"<<isSorted(first)<<endl;
//  free(first);
//
//  int Duplicates[] = {3,5,5,8,8};
//  create(Duplicates,5);
//
//  cout<<"Duplicates"<<endl;
//  Display(first);
//  cout<<"Removing Duplicates"<<endl;
//  removeDuplicates(first);
//  cout<<"Result"<<endl;
//  Display(first);
//  recursiveReverse(NULL,first);
//  Display(first);
    int n;
    cin>>n;
	return 0;
}