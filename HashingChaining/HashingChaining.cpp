// HashingChaining.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <iostream>
#include "LinkedList.h";

using namespace std;

// Hash function
int Hash(int key){
    return key%10;
}

// insert element at given index 
void Insert(struct Node **H,int key){
    int index = Hash(key);

	SortedInsert(&H[index],key);
}



int main()
{
    // Creating array of pointers
    struct Node *HT[10];
	struct Node* temp;

    for(int i=0; i<10;i++){
        HT[i] = NULL;
    }

    Insert(HT,12);
    Insert(HT,22);
    Insert(HT,42);

	temp = Search(HT[Hash(22)],22 );
    cout<<temp->data<<endl;
}

// Ejecutar programa: Ctrl + F5 o menú Depurar > Iniciar sin depurar
// Depurar programa: F5 o menú Depurar > Iniciar depuración

// Sugerencias para primeros pasos: 1. Use la ventana del Explorador de soluciones para agregar y administrar archivos
//   2. Use la ventana de Team Explorer para conectar con el control de código fuente
//   3. Use la ventana de salida para ver la salida de compilación y otros mensajes
//   4. Use la ventana Lista de errores para ver los errores
//   5. Vaya a Proyecto > Agregar nuevo elemento para crear nuevos archivos de código, o a Proyecto > Agregar elemento existente para agregar archivos de código existentes al proyecto
//   6. En el futuro, para volver a abrir este proyecto, vaya a Archivo > Abrir > Proyecto y seleccione el archivo .sln
