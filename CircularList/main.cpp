#include <iostream>


using namespace std;

struct Node{
  int data;
  struct Node *next;
} *Head;

void create(int A[],int n){
  int i;
  struct Node *t,*last;

  Head = (struct Node *)malloc(sizeof(struct Node));
  Head->data = A[0];
  Head->next = Head;
  
  last = Head;
  for(i=1; i<n; i++){
    t= (struct Node *)malloc(sizeof(struct Node));
    t->data = A[i];
    t->next = last->next;
    last->next = t;
    last = t;
  }

}

void Display(struct Node *p){
  struct Node *q;
  q=p;
  do{
    cout<<p->data<<endl;
    p = p->next;
    q = q->next;
    if (q->next){
      q=q->next;
    }
    
  }while(p&&q && p != q );
  
}

void RecursiveDisplay(struct Node *h){

  static int flag=0;
  if(h != Head || flag ==0){
    flag = 1;
    cout<<h->data<<endl;
    RecursiveDisplay(h->next);
  }
  flag = 0;

}

int length(struct Node *p){
  struct Node *q;
  q=p;
  int len =0;
  do{
    
    p = p->next;
    q = q->next;
    if (q->next){
      q=q->next;
    }
    len++;
    
  }while(p&&q && p != q );
  return len;
}

void insert(struct Node *p,int index,int data){
  if (index < 0 || index > length(p) ) {
    return;
  }

  struct Node *t = (struct Node*) malloc(sizeof(struct Node));
  
  t->data = data;

  if (index == 0){
    if (Head == NULL){
      Head = t;
      Head->next = Head;
    }
    else {

      while(p->next != Head){
        p = p->next;
      }
      p->next = t;
      t->next = Head;
      Head = t;
    }

    
  }

  else {
    for(int i=0; i<index-1; i++){
      p = p->next;
    }
    
    t->next = p->next;
    p->next = t;
  }
}

int Delete( struct Node *p,int index ){
  struct Node *q;
  int x;
  if (index < 0 || index > length(Head)){
    return -1;
  }
  if (index ==1){
    while (p->next != Head) p=p->next;
    x= Head->data;
    if (Head ==p){
      free(Head);
      Head = NULL;
    }
    else {
      p->next = Head->next;
      free(Head);
      Head = p->next;
    }

  }
  else {
    for(int i =0; i<index-2;i++)
      p=p->next;
    
    q= p->next;
    p->next = q->next;
    x=q->data;
    free(q);
  }
  return x;
}


int main(){
	
  int A[] = {2,3,4,5,6};
  create(A,5);
  Display(Head);
  insert(Head,1,9);
  cout<<"display"<<endl;
  Display(Head);
  Delete(Head,2);
  cout<<"display"<<endl;
  Display(Head);
	return 0;
}