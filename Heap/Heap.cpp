// Heap.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <iostream>
using namespace std;

// N represent the index in H array
// The insert comes from the latest elemnt to the beginning
void Insert(int H[],int n){
    int i= n,temp;
    // value to insert
    temp = H[i];


    while (i>1 && temp > H[i/2]){
        H[i] = H[i/2];
        i = i/2;
    }
    H[i] = temp;

}
// N represent the number of elements in A array

int Delete(int A[],int n){
    int i=0,j=0,x=0,temp=0,val=0;

    // getting the root deleted (you can only delete root)
    val = A[1];

    // Assigning the last element into a temp
    x = A[n];

    // Copying the last element into the first element
    A[1] = A[n];

    i=1;
    // pointing into left child
    j=i*2;

    // while n-1 because we are not taking the last index
    while(j < n-1){
        // Comparing the right child with the left child
        if (A[j +1 ] > A[j] ){
            j = j+1;
        }

        // Checking if it is less than the child then swap
        if ( A[i]<A[j]){
            temp = A[i];
            A[i] = A[j];
            A[j] = temp;
            // Updating value of i to the current j
            i = j;
            // Moving j to left child
            j = 2*j;
        }
        // if i is not less then finish and get out of the loop
        else {
            break;
        }
    }
    // Moving the deleted value to last element
    A[n] = val;

    return val;
}

int main()
{
    // Max Heap
    //  ignore the first index
    int heap[] = {0,10,20,30,25,5,40,35};
    
    // Inserting elements
	for (int i = 2; i <= 7; i++) {
        Insert(heap,i);
	}
    // Result after inserting
    // 40,25,35,10,5,20,30
	
    for (int i = 1; i <= 7; i++) {
        cout << heap[i]<<endl;
	}

    // // Deleting 40
    // Delete(heap,7);
    for (int i = 7; i > 1; i--) {
        cout <<Delete(heap,i)<<endl;
	}

    cout << "Delete result"<<endl;
    for (int i = 1; i <= 7; i++) {
        cout << heap[i]<<endl;
	}

}

// Ejecutar programa: Ctrl + F5 o menú Depurar > Iniciar sin depurar
// Depurar programa: F5 o menú Depurar > Iniciar depuración

// Sugerencias para primeros pasos: 1. Use la ventana del Explorador de soluciones para agregar y administrar archivos
//   2. Use la ventana de Team Explorer para conectar con el control de código fuente
//   3. Use la ventana de salida para ver la salida de compilación y otros mensajes
//   4. Use la ventana Lista de errores para ver los errores
//   5. Vaya a Proyecto > Agregar nuevo elemento para crear nuevos archivos de código, o a Proyecto > Agregar elemento existente para agregar archivos de código existentes al proyecto
//   6. En el futuro, para volver a abrir este proyecto, vaya a Archivo > Abrir > Proyecto y seleccione el archivo .sln
